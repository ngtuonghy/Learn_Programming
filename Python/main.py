def search(list, target):
    l = 0 
    r = len(list) - 1

    while l <= r:
        midpoint = (l + r)//2
        if list[midpoint]:
            return midpoint
        elif list[midpoint] < target:
            l = midpoint +1
        elif list[midpoint] > target:
            r = midpoint -1
        else:
            return None

def Print(index):
    if index is not None:
        print("target not found in index", index)
    else:
        print("find susscess")


arr = [1,2,3,4,5,6,7,8,9]
result = search(arr,0)
Print(result)

