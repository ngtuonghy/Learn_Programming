#include <iostream>
void insertion(int arr[], int n) {
  for (int i = 1; i < n; i++) {
    int key = arr[i];
    int j = i - 1;
    while (j >= 0 && arr[j] > key) {
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = key;
  }
}
int main(int argc, char *argv[]) {
  int arr[] = {
      3, 4, 6, 7, 4,
  };
  insertion(arr, 5);
  for (auto x : arr) {
    std::cout << x << " ";
  }
  return 0;
}
